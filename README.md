# PACKER.IO templates for DigitalOcean instances #

This repo contains templates to create provisioned images on DigitalOcean. The images are divided up in basic servers that only have the bare minimum installed for security as well as some other tweaks to make them more robust and secure.
Others will be deployed with a basic web server like Apache or Nginx or maybe with PostgreSQL or MariaDB.

This is very much work in progress and a little of a playground, proceed with caution!
 
### Summary ###

* Packer.io templates for DigitalOcean images.
* Version: v0.1
* Images:
    - ubuntu: This is just a basic Ubuntu server 14.04 image
    - centos: This is just a basic Centos 7.2 server image


### How do I get set up? ###

* First install Packer (www.packer.io)
* Test if Packer works with "packer --version"
* Log into your DigitalOcean account or create a new one
* Create a new API token (make it writeable)
* Run "packer build -var "api_token=XXXXX" template.json" from the directory of the image you want.
* Follow the screen for any issues.
