#!/bin/bash

# updating the system
yum update -y

# installing some basic tools
yum install -y epel-release
yum install -y fail2ban
yum install -y vim
yum install -y git

# Getting a familiar environment
cd ~
git clone git@bitbucket.org:zephyrict/dotfiles.git
mv dotfiles .dotfiles
cd .dotfiles
./mksymlinks.sh


# Disabling IPv6
cat <<EOT>> /etc/sysctl.conf
# Disable ipv6
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
EOT




