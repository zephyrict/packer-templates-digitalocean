#!/bin/bash

# Setting up and configuring fail2ban
cat /etc/fail2ban/jail.conf >> /etc/fail2ban/jail.local
sed -i "s/^usedns[[:blank:]]*= warn/usedns = no/" /etc/fail2ban/jail.local
sed -i "s/^bantime[[:blank:]]*= 600/bantime = 3600/" /etc/fail2ban/jail.local
sed -i "/^\[ssh\]$/,/^\[/s/enabled[[:blank:]]*= false/enabled = true/" /etc/fail2ban/jail.local
sed -i "/^\[ssh\]$/,/^\[/s/maxretry[[:blank:]]*= 6/maxretry = 3/" /etc/fail2ban/jail.local
sed -i "/^\[ssh-ddos\]$/,/^\[/s/enabled[[:blank:]]*= false/enabled = true/" /etc/fail2ban/jail.local
sed -i "/^\[ssh-ddos\]$/,/^\[/s/maxretry[[:blank:]]*= 6/maxretry = 3/" /etc/fail2ban/jail.local

# Enabling Firewalld and allowing some ports
systemctl enable firewalld
systemctl start firewalld
firewall-cmd --set-default-zone=external
firewall-cmd --permanent --zone=external --add-service=http
firewall-cmd --permanent --zone=external --add-service=https

