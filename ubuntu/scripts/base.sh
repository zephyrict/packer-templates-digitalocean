#!/bin/bash

# updating repos and upgrading the system
apt-get update
apt-get -y upgrade

# installing some basic tools
apt-get -y install fail2ban
apt-get -y install unattended-upgrades


# Disabling IPv6
sed -i 's,^#precedence ::ffff:0:0/96  100,precedence ::ffff:0:0:/96  100,' /etc/gai.conf


