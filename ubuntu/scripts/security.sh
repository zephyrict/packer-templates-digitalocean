#!/bin/bash

# Setting up and configuring fail2ban
cat /etc/fail2ban/jail.conf >> /etc/fail2ban/jail.local
sed -i "s/^usedns[[:blank:]]*= warn/usedns = no/" /etc/fail2ban/jail.local
sed -i "s/^bantime[[:blank:]]*= 600/bantime = 3600/" /etc/fail2ban/jail.local
sed -i "/^\[ssh\]$/,/^\[/s/enabled[[:blank:]]*= false/enabled = true/" /etc/fail2ban/jail.local
sed -i "/^\[ssh\]$/,/^\[/s/maxretry[[:blank:]]*= 6/maxretry = 3/" /etc/fail2ban/jail.local
sed -i "/^\[ssh-ddos\]$/,/^\[/s/enabled[[:blank:]]*= false/enabled = true/" /etc/fail2ban/jail.local
sed -i "/^\[ssh-ddos\]$/,/^\[/s/maxretry[[:blank:]]*= 6/maxretry = 3/" /etc/fail2ban/jail.local

# Configuring unattended-upgrades
sudo cp -f /tmp/10periodic /etc/apt/apt.conf.d/
sudo cp -f /tmp/20auto-upgrades /etc/apt.conf.d/

# disable ipv6 support for ufw
sed -i "s/^IPV6=yes/ipv6=no/" /etc/default/ufw 

# Enabling ufw and allowing some ports
sudo ufw enable
sudo ufw allow ssh
sudo ufw allow 80
sudo ufw allow 443
